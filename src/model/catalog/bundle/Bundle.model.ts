import {ITimeStampedDocument} from "../../../plugins/timestamp-plugin";
import {Nation} from "../../common/Location.model";
import {BUNDLE_STATE} from "./BundleState.enum";
import {LocalizationModel} from "../../common/localization.model";

export interface IBundleItems extends ITimeStampedDocument {
    /** ServiceId of the service */
    serviceId: string;
    /** Price of the bundle item */
    price: number;
    /** Quantity of the bundle item */
    quantity: number;
}

export interface IBundleApprovalProperty extends ITimeStampedDocument {
    key: string;
    value: any;
    status: string;
    approvedBy: string;
}

export interface IBundleApproval extends ITimeStampedDocument {
    author: string;
    status: string;
    properties: IBundleApprovalProperty[]
}

export interface IBundle extends ITimeStampedDocument {
    /** Code of the bundle */
    code: string;
    /** Name of the bundle */
    name: LocalizationModel[];
    /** Locations of the bundle */
    nations: Nation[];
    /** Price of the bundle */
    price: number;
    /** items of the bundle */
    items: IBundleItems[]
    /** UserId of admin user which creates the bundle */
    createdBy: string;
    /** Type of the bundle */
    type: string;
    /** State of the bundle */
    state: BUNDLE_STATE;
    /** Approvals of the bundle */
    approvals: IBundleApproval[];

}
