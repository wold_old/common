export enum BUNDLE_STATE {
    ACTIVE = 'ACTIVE',
    PENDING = 'PENDING',
    APPROVED = 'APPROVED'
}
