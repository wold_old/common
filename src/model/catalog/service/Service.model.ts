import {ITimeStampedDocument} from "../../../plugins/timestamp-plugin";
import {SERVICE_TYPE} from "./ServiceType.enum";
import {USER_ROLES} from "../../../enum/roles.enum";
import {LocalizationModel} from "../../common/localization.model";

export interface IService extends ITimeStampedDocument {
    /** Name of the service */
    name: LocalizationModel[];
    /** Code of the service */
    code: string;
    /** Type of the service */
    types: SERVICE_TYPE[];
    /** UserId of the admin user which creates the service */
    createdBy: string;
    /** Price of the service */
    price: number;
    /** Renewal duration of the service (month) */
    renewalDuration: number;
    /** User roles that can view the service */
    visibleFor: USER_ROLES[];
    /** User roles unlocked with the service's purchase */
    roles: USER_ROLES[];
    /** Expire Date of the service (millis) **/
    expiredDate: number;
}
