import {ORDER_STATE} from "./OrderState.enum";
import {ICart} from "../catalog/order/Cart.model";

export interface Order {
    state: ORDER_STATE;
    paymentId: string;
    cart: ICart;
    invoiceId: string;
    userId: string;
    totAmount: number;
    date: Date;
}
