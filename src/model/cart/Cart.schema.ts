import {Schema} from "mongoose";

export const cartSchema = new Schema({
    serviceItems: {type: Schema.Types.Array},
    bundleItems: {type: Schema.Types.Array},
    totAmount: {type: Number}
});
