import {ITimeStampedDocument} from "../../../plugins/timestamp-plugin";
import {IStructure} from "./Structure.model";

export interface ICorporate extends ITimeStampedDocument {
    /** Total number of structures */
    totStructure: number;
    /** Tot Square meter of the structures */
    totSquareMeter: number;
    /** List of Customer's structure */
    structures: IStructure[];
    /** Typology of the Corporate's structures */
    structureTypologies: string[];
    type: string; //PARTNER or COSTUMER
    services: string[];
}
