import {ITimeStampedDocument} from "../../../plugins/timestamp-plugin";
import {Location} from "../../common/Location.model";
import {ICorporate} from "./Corporate.model";
import {IFreelance} from "./Freelance.model";

export interface IPartner extends ITimeStampedDocument {
    /** Iban of the Partner **/
    iban: string;
    /** mobilePhone of the Partner **/
    mobilePhone: string;
    /** telephone of the Partner **/
    telephone: string;
    /** Location of the Customer */
    location: Location;
    /** Numer of the partner's users */
    numberOfUsers: number;
    /** Type of the Partner */
    type: string;

    corporate: ICorporate;
    freelance: IFreelance;
}
