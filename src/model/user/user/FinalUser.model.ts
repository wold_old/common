import {ITimeStampedDocument} from "../../../plugins/timestamp-plugin";
import {Location} from "../../common/Location.model";

export interface IFinalUser extends ITimeStampedDocument {
    /** Name of the Consumer **/
    name: string;
    /** Surname of the Consumer **/
    surname: string;
    /** Date of birth of the Consumer **/
    dateOfBirth: Date;
    /** Gender of the Physical Consumer */
    gender: string;
    /** Fiscal Code of the Physical Consumer */
    fiscalCode: string;
    /** telephone of the Physical Consumer */
    telephone: string;
    /** Profession of the Physical Consumer */
    profession: string;
    /** Doctor of the Physical Consumer */
    doctor: string;
    /** Location of the Customer */
    location: Location;
}
