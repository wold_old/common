import {ITimeStampedDocument} from "../../plugins/timestamp-plugin";
import {IUser} from "../user/User";
import {USER_ROLES} from "../../enum/roles.enum";

export interface IInvite extends ITimeStampedDocument {
    guest: IUser;
    inviting: IUser;
    email: string;
    guestRoles: USER_ROLES[];
    link: string;
    expirationDate: number;
    accepted: boolean;
}
