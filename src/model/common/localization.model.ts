export interface LocalizationModel {
    value: string;
    locale: string;
}
