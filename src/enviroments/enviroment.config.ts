export const ENVIRONMENT_CONFIG = {
  default: 'enviroments/.env.default',
  development: 'enviroments/.env.development',
  staging: 'enviroments/.env.staging',
  production: 'enviroments/.env.production'
};
