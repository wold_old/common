import {
  Request, Response, NextFunction
} from 'express';
import * as jwt from 'jsonwebtoken';
import * as _ from 'lodash';

export const authenticate = (roles: string[] = []) => (req: Request, response: Response, next: NextFunction) => {
  const authHeader = req.headers['authorization'];
  const token = authHeader && authHeader.split(' ')[1];

  if (token == null) return response.sendStatus(401);

  // eslint-disable-next-line consistent-return
  jwt.verify(token, process.env.ACCESS_SECRET as string, (err: any, user: any) => {
    // eslint-disable-next-line max-len
    const validRoles = roles.length === 0 || _.intersection(user.roles, roles).length > 0;
    if (err || !validRoles) {
      return response.sendStatus(403);
    }

    _.set(req, 'user', user);
    return next();
  });
};

export default authenticate;
