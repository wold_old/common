"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.authenticate = void 0;
const jwt = __importStar(require("jsonwebtoken"));
const _ = __importStar(require("lodash"));
const authenticate = (roles = []) => (req, response, next) => {
    const authHeader = req.headers['authorization'];
    const token = authHeader && authHeader.split(' ')[1];
    if (token == null)
        return response.sendStatus(401);
    // eslint-disable-next-line consistent-return
    jwt.verify(token, process.env.ACCESS_SECRET, (err, user) => {
        // eslint-disable-next-line max-len
        const validRoles = roles.length === 0 || _.intersection(user.roles, roles).length > 0;
        if (err || !validRoles) {
            return response.sendStatus(403);
        }
        _.set(req, 'user', user);
        return next();
    });
};
exports.authenticate = authenticate;
exports.default = exports.authenticate;
