import { Request, Response, NextFunction } from 'express';
export declare const authenticate: (roles?: string[]) => (req: Request, response: Response, next: NextFunction) => Response<any, Record<string, any>>;
export default authenticate;
