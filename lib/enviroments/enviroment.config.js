"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ENVIRONMENT_CONFIG = void 0;
exports.ENVIRONMENT_CONFIG = {
    default: 'enviroments/.env.default',
    development: 'enviroments/.env.development',
    staging: 'enviroments/.env.staging',
    production: 'enviroments/.env.production'
};
