export declare const ENVIRONMENT_CONFIG: {
    default: string;
    development: string;
    staging: string;
    production: string;
};
