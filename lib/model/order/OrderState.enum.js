"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ORDER_STATE = void 0;
var ORDER_STATE;
(function (ORDER_STATE) {
    ORDER_STATE["PAID"] = "PAID";
    ORDER_STATE["WAITING_FOR_PAYMENT"] = "WAITING_FOR_PAYMENT";
    ORDER_STATE["PENDING"] = "PENDING";
})(ORDER_STATE = exports.ORDER_STATE || (exports.ORDER_STATE = {}));
