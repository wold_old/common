export declare enum ORDER_STATE {
    PAID = "PAID",
    WAITING_FOR_PAYMENT = "WAITING_FOR_PAYMENT",
    PENDING = "PENDING"
}
