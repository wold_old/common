"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.cartSchema = void 0;
const mongoose_1 = require("mongoose");
exports.cartSchema = new mongoose_1.Schema({
    serviceItems: { type: mongoose_1.Schema.Types.Array },
    bundleItems: { type: mongoose_1.Schema.Types.Array },
    totAmount: { type: Number }
});
