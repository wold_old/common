import { IService } from "../service/Service.model";
import { IBundle } from "../bundle/Bundle.model";
import { ITimeStampedDocument } from "../../../plugins/timestamp-plugin";
export interface ICartBundleItem extends ITimeStampedDocument {
    bundle: IBundle;
    quantity: number;
    totAmount: number;
}
export interface ICartServiceItem extends ITimeStampedDocument {
    service: IService;
    quantity: number;
    totAmount: number;
}
export interface ICart extends ITimeStampedDocument {
    serviceItems: ICartServiceItem[];
    bundleItems: ICartBundleItem[];
    totAmount: number;
}
