"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SERVICE_TYPE = void 0;
var SERVICE_TYPE;
(function (SERVICE_TYPE) {
    SERVICE_TYPE["COSTUMER"] = "CUSTOMER";
    SERVICE_TYPE["SPECIALIST"] = "SPECIALIST";
    SERVICE_TYPE["ALL"] = "ALL";
    SERVICE_TYPE["PARTNER"] = "PARTNER";
    SERVICE_TYPE["MULTI_PLAYER"] = "MULTI_PLAYER";
    SERVICE_TYPE["SALE"] = "SALE";
})(SERVICE_TYPE = exports.SERVICE_TYPE || (exports.SERVICE_TYPE = {}));
