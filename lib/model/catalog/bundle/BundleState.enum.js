"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BUNDLE_STATE = void 0;
var BUNDLE_STATE;
(function (BUNDLE_STATE) {
    BUNDLE_STATE["ACTIVE"] = "ACTIVE";
    BUNDLE_STATE["PENDING"] = "PENDING";
    BUNDLE_STATE["APPROVED"] = "APPROVED";
})(BUNDLE_STATE = exports.BUNDLE_STATE || (exports.BUNDLE_STATE = {}));
