import { ITimeStampedDocument } from "../../../plugins/timestamp-plugin";
import { Location } from "../../common/Location.model";
import { IFreelance } from "./Freelance.model";
import { ICorporate } from "./Corporate.model";
export interface ICustomer extends ITimeStampedDocument {
    /** Structure name of the Customer */
    structureName: string;
    /** VAT number of the Customer */
    vatNumber: string;
    /** SDI of the Customer */
    sdi: string;
    /** PEC email of the Customer */
    pec: string;
    /** Iban of the Customer */
    iban: string;
    /** telephone of the Customer */
    telephone: string;
    /** Type of the Customer */
    type: string;
    /** Location of the Customer */
    location: Location;
    /** Number of years the activity is active */
    activityYears: number;
    corporate: ICorporate;
    freelance: IFreelance;
}
