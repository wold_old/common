import { ITimeStampedDocument } from "../../../plugins/timestamp-plugin";
import { Location } from "../../common/Location.model";
export interface IAmbassador extends ITimeStampedDocument {
    /** Name of the Sale **/
    name: string;
    /** Surname of the Sale **/
    surname: string;
    /** Date of birth of the Sale **/
    dateOfBirth: Date;
    /** Gender of the Physical Sale */
    gender: string;
    /** Fiscal Code of the Physical Sale */
    fiscalCode: string;
    /** telephone of the Physical Sale */
    telephone: string;
    /** Profession of the Physical Sale */
    profession: string;
    /** Location of the Customer */
    location: Location;
}
