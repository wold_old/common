import { ITimeStampedDocument } from "../../../plugins/timestamp-plugin";
export interface IFreelance extends ITimeStampedDocument {
    /** Registration number in the professional register */
    registrationNumber: number;
    /** Tot Square meter of the structures */
    taxRegime: string;
    /** Typology of the Freelance */
    typology: string;
    type: string;
    services: string[];
}
