import { ITimeStampedDocument } from "../../../plugins/timestamp-plugin";
import { IService } from "../../catalog/service/Service.model";
import { ICart } from "../../catalog/order/Cart.model";
export interface IStructure extends ITimeStampedDocument {
    type: string;
    totRevenue: number;
    squareMeter: number;
    /** Services purchased through a bundle  **/
    services: IService[];
    typology: string;
    cart: ICart;
}
