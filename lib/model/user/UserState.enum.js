"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.USER_STATE = void 0;
var USER_STATE;
(function (USER_STATE) {
    USER_STATE["ACTIVE"] = "ACTIVE";
    USER_STATE["CHANGE_PASSWORD"] = "CHANGE_PASSWORD";
    USER_STATE["WAITING_FOR_ACTIVATION"] = "WAITING_FOR_ACTIVATION";
    USER_STATE["PENDING"] = "PENDING";
    USER_STATE["REFUSED"] = "REFUSED";
    USER_STATE["FIRST_ACCESS"] = "FIRST_ACCESS";
    USER_STATE["INACTIVE"] = "INACTIVE";
})(USER_STATE = exports.USER_STATE || (exports.USER_STATE = {}));
