import { ITimeStampedDocument } from "../../plugins/timestamp-plugin";
import { LocalizationModel } from "../common/localization.model";
import { Location } from "../common/Location.model";
import { IService } from "../catalog/service/Service.model";
import { ICart } from "../catalog/order/Cart.model";
import { IFinalUser } from "./user/FinalUser.model";
import { IPartner } from "./user/Partner.model";
import { IAmbassador } from "./user/Ambassador.model";
import { ICorporate } from "./user/Corporate.model";
export interface IRegion extends ITimeStampedDocument {
    name: LocalizationModel[];
    nations: Location[];
}
export interface ILocation extends ITimeStampedDocument {
    /** Address of the user */
    address: string;
    /** City of the user */
    city: string;
    /** StreetNumber of the user */
    streetNumber: number;
    /** Zipcode of the user */
    zipCode: string;
    /** Province of the user */
    province: string;
    /** Location of the user */
    nation: Location;
}
export interface IPromoter extends ITimeStampedDocument {
    /** Iban of the Promoter **/
    iban: string;
    /** mobilePhone of the Promoter **/
    mobilePhone: string;
    /** telephone of the Promoter **/
    telephone: string;
}
export interface IAdmin extends ITimeStampedDocument {
    /** ParentId of the current Admin **/
    parentId: string;
    /** Region of of the Admin */
    region: IRegion;
}
export interface ISpecialist extends ITimeStampedDocument {
    /** Order date of the Specialist */
    orderDate: string;
    /** Location of the Specialist */
    location: Location;
    /** Services of the Specialist */
    services: IService[];
}
export interface IUser extends ITimeStampedDocument {
    /** Username of the user */
    username: string;
    /** Password of the user */
    password: string;
    /** Email of the user */
    email: string;
    /** telephone of user */
    telephone: string;
    /** admin registry of user */
    admin: IAdmin;
    /** Roles of user */
    roles: string[];
    /** User cart */
    cart: ICart;
    /** Location of the user */
    location: ILocation;
    /** Partner registry of the user */
    partner: IPartner;
    corporate: ICorporate;
    /** Promoter registry of the user */
    promoter: IPromoter;
    /** Sales registry of the user */
    ambassador: IAmbassador;
    /** Consumer registry of the user */
    finalUser: IFinalUser;
    /** Specialist registry of the user */
    specialist: ISpecialist;
}
