"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const TimeStampPlugin = function (schema) {
    schema.add({ createdAt: { type: Number, index: true } });
    schema.add({ updatedAt: { type: Number, index: true } });
    schema.pre('save', function (next) {
        if (this.isNew) {
            this.createdAt = new Date().getTime();
        }
        this.updatedAt = new Date().getTime();
        next();
    });
};
exports.default = TimeStampPlugin;
