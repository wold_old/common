import { Document, Schema } from 'mongoose';
export interface ITimeStampedDocument extends Document {
    /** Timestamp at creation in milliseconds */
    createdAt: number;
    /** Timestamp at updation in milliseconds */
    updatedAt: number;
}
declare const TimeStampPlugin: (schema: Schema) => void;
export default TimeStampPlugin;
